﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace ConsoleApplication1
{
    public class DirectoryExplorer
    {
        private string[] commandArray = { "step", "parent", "root", "list", "run", "rename" }; 
        private string currentCommand;
        private string[] currentParameters;
        private string currentPath = "c:\\";
        private bool singleCommand = true;

        public void run(string userValue)
        {
            try
            {
                setupCommands(userValue);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw;
            }

            switch (currentCommand)
            {
                case "step":
                    runStepCommand();
                    break;
                case "parent":
                    runParentCommand();
                    break;
                case "root":
                    runRootCommand();
                    break;
                case "list":
                    runListCommand();
                    break;
                case "run":
                    runRunCommand();
                    break;
                case "rename":
                    runRenameCommand();
                    break;
                default:
                    break;
            }

            Console.WriteLine("Path is: {0}", currentPath);
            
            
        }
        private void runRenameCommand()
        {
            if (currentParameters.Length >= 2)
            {
                Directory.Move(currentPath + currentParameters[0], currentPath + currentParameters[1]);
            }
        }
        private void runParentCommand()
        {
            if (!(currentPath == "c:\\"))
            {
                currentPath = Directory.GetParent(currentPath).FullName;
                removeParameters();
                runStepCommand();
            }
        }
        private void runRootCommand()
        {
            currentPath = "c:\\";
            removeParameters();
            runStepCommand();
        }
        private void runRunCommand()
        {
            if (File.Exists(currentPath + currentParameters[0]))
            {
                Process myProcess = new Process();
                myProcess.StartInfo.FileName = currentPath + currentParameters[0];
                myProcess.Start();
            }
        }
        private void removeParameters()
        {
            currentParameters = null;
            singleCommand = true;
        }
        private void runStepCommand()
        {

            if (singleCommand == false)
            {

                string tempPath = currentPath + currentParameters[0];

                if (Directory.Exists(tempPath))
                {
                    currentPath = tempPath;
                }
            }
            
            
        }
        private void runListCommand()
        {
            string[] dirs = getDirectories(currentPath);
            string[] files = getFiles(currentPath);

            Console.ForegroundColor = ConsoleColor.Green;
            int count = 0;
            foreach(string dir in dirs){
                string printableDir = "*" + stripParentDirs(dir) + " ";
                Console.Write(printableDir);

                if (count == 3)
                {
                    Console.WriteLine();
                    count = 0;
                }
                else
                {
                    count++;
                }
            }
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            count = 0;
            foreach (string file in files)
            {

                string printableFile = stripParentDirs(file) + " ";
                Console.Write(printableFile);
                
                if (count == 3)
                {
                    Console.WriteLine();
                    count = 0;
                }
                else
                {
                    count++;
                }
            }
            Console.WriteLine();
        }
        private string stripParentDirs(string path)
        {
            int index;
            for (int i = path.Length - 1; i > 0; i--)
            {
                if (path.Substring(i, 1) == "\\")
                {
                    return path.Substring(i + 1);
                }
            }
            return path;
        }
        private string[] getDirectories(string path)
        {
            string[] directories = Directory.GetDirectories(path);
            return directories;
        }
        private string[] getFiles(string path)
        {
            string[] files = Directory.GetFiles(path);
            return files;
        }

        private void setupCommands(string userValue)
        {
            string[] tempArray = splitCommand(userValue);
            
            if (!isSingleCommand(tempArray))
            {
                singleCommand = false;
                string[] parameters = splitParameters(tempArray);
                currentParameters = parameters;
            }
            currentCommand = tempArray[0];
            if (!checkCommandExists(currentCommand))
            {
                throw new ArgumentOutOfRangeException("Command must be either 'open', 'pare' or 'root'.");
            }
        }

        private bool checkCommandExists(string userCommand)
        {
            foreach (string command in commandArray)
            {
                if (command == userCommand) return true;
            }
                return false;
        }

        private void setCommand(string userCommand)
        {
            this.currentCommand = userCommand;
        }

        private string[] splitCommand(string userValue)
        {
            List<string> commands = new List<string>();
            int index = 0;
            string tempString = "";

            userValue += " "; //TODO fix space bug

            while (index < userValue.Length)
            {
                if (userValue.Substring(index, 1) == " ")
                {
                    commands.Add(tempString);
                    tempString = "";
                }
                else
                {
                    tempString += userValue.Substring(index, 1);
                }
                index++;
            }

            string[] s = commands.ToArray();
            return s;
        }



        public static void Main()
        {

            DirectoryExplorer de = new DirectoryExplorer();
            string commands = "step";
            de.run(commands);
            while (true)
            {
                de.run(Console.ReadLine());
            }
            

        }
        private string[] splitParameters(string[] commands)
        {
            List<string> parameters = new List<string>();

            for (int i = 1; i < commands.Length; i++)
            {
                if (!(commands[i].Substring(0, 1) == "\\")) commands[i] = "\\" + commands[i];
                parameters.Add(commands[i]);
            }

            return parameters.ToArray();
        }
        private bool isSingleCommand(string[] command)
        {
            if (command.Length == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}
